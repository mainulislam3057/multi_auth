<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Showing All Posts to admin 
     */
    public function show_posts()
    {
        $blogs = Post::OrderBy('updated_at', 'DESC')->get();
        return view('dashboard.admin.manage_posts',[
            'blogs' => $blogs
        ]);
    }

    /**
     * Showing All Users to admin 
     */
    public function show_users()
    {
        $users = User::OrderBy('updated_at', 'DESC')->get();
        return view('dashboard.admin.manage_users',[
            'users' => $users
        ]);
    }

    /**
     * Delete Specific Post from admin dashboard
     */
    public function post_delete($id)
    {

        $post = Post::where('id', $id)->first();
        Storage::disk('public')->delete('images/'.$post->image_path);
        $post->delete();
        return redirect()->back()->with('message', 'Blog has been deleted!');
    }

    /**
     * Delete Specific User from admin dashboard
     */

    public function user_delete($id)
    {

        $post = User::where('id', $id)->first();
        $post->delete();
        return redirect()->back()->with('message', 'User has been deleted!');
    }

    /**
     * Viewing Specific Post from admin dashboard
     */

    public function single_post($id)
    {

        $post = Post::where('id', $id)->first();
        return view('dashboard.admin.view_post', [
            'post' => $post
        ]);

    }

    /**
     * Post Approval/Decline from admin dashboard
     */
    public function post_status(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',

        ]);

        Post::where('id', $id)->update([
            'status' => $request->status,
        ]);
        return redirect()->back()->with('message', 'Blog Status updated!');
    }

    public function report_get(Request $request)
    {
        // $from = date('2022-06-19');
        // $to = date('2022-06-20');
        // $result = Post::whereBetween('created_at', [$from, Carbon::parse($to)->endOfDay(),])->where('user_id', 1)->get();
        
        // $from = $request->from;
        // $to = $request->to;

        // $user = $request->user;
        // $status_val = $request->status;

        
        $from = request('from', null);
        $to = request('to',null);
        $user = request('user',null);
        $status_val = request('status', null);
        $type = request('type', 'comment');
        
        $users = User::all();
        
        // $results = Post::withCount(['comments' => function ($query) use($from,$to){
        //     $query->whereBetween('comments.created_at', [$from, Carbon::parse($to)->endOfDay(),]);
        // }])->where('user_id', $user)->where('status', $status_val)->get();

        $results = Post::when($request->has('user'), function($query) use($user){
            $query->where('user_id', $user);
        })
        ->when($request->has('status'), function($query) use($status_val){
            $query->where('status', $status_val);
        })
        ->when($from && $to, function($query) use($from, $to, $type){
            if($type == 'post') {
                $query->whereBetween('posts.created_at', [$from.' 00:00:00', $to.' 23:59:59'])->withCount('comments');
            } else {
                $query->withCount(['comments'=> function($q)use($from, $to){
                    $q->whereBetween('comments.created_at', [$from.' 00:00:00', $to.' 23:59:59']);
                }]);
            }
        })
        ->paginate(1);

        return view('dashboard.admin.report', [
            'results' => $results,
            'users' => $users,
        ]);
        return dd($results);
    }

    // public function report_result(Request $request)
    // {


    //     $from = $request->from;
    //     $to = $request->to;

    //     // $from = date('2022-06-22');
    //     // $to = date('2022-06-22');

    //     $user = $request->user;
    //     $status_val = $request->status;
    //     // $results = Post::whereBetween('created_at', [$from, Carbon::parse($to)->endOfDay(),])->where('user_id', $user)->get();
        
    //     $users = User::all();

    //     // $results = Post::whereHas('comments', function($q) use($from,$to){
    //     //     $q->whereBetween('created_at', [$from, Carbon::parse($to)->endOfDay(),]);
    //     // })->get();
    //         $results = Post::withCount(['comments' => function ($query) use($from,$to){
    //             $query->whereBetween('comments.created_at', [$from, Carbon::parse($to)->endOfDay(),]);
    //         }])->where('user_id', $user)->where('status', $status_val)->get();

    //     return view('dashboard.admin.report', [
    //         'results' => $results,
    //         'users' => $users,
    //     ]);

    //     // return dd($test);
    // }

}