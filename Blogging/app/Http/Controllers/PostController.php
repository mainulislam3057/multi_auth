<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('welcome')->with('posts', Post::orderBy('updated_at', 'DESC')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.user.create_blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg|max:5084',
        ]);

        $ImageName = uniqid() . '-' . $request->title . '.' . $request->image->extension();

        Storage::disk('public')->put('images/'.$ImageName, file_get_contents($request->image->getRealpath()));
        
        $slug_count = Post::where('title', $request->title)->count();
        $slug_count += 1;
        $slug = Str::slug($request->title, "-");

        Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'slug' => $slug."-".$slug_count,
            'image_path' => $ImageName,
            'user_id' => auth()->user()->id
        ]);

        return redirect()->back()->with('message', "Post Created Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('dashboard.user.view', [
            'post' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('dashboard.user.edit', [
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',

        ]);

        $ImageName = '';

        if ($request->hasFile('new_image')) {
            $ImageName = uniqid() . '-' . $request->title . '.' . $request->new_image->extension();
            Storage::disk('public')->put('images/'.$ImageName, file_get_contents($request->new_image));
            // unlink('blog_images/' . $request->image);
            Storage::disk('public')->delete('images/'.$request->old_image);

        } else {
            $ImageName = $request->old_image;
        }

        Post::where('slug', $slug)->update([
            'title' => $request->title,
            'description' => $request->description,
            'slug' => Str::slug($request->title, "-"),
            'image_path' => $ImageName,
            'user_id' => auth()->user()->id
        ]);
        return redirect()->back()->with('message', 'Your Blog updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $post = Post::where('slug', $slug)->first();
        Storage::disk('public')->delete('images/'.$post->image_path);
        $post->delete();
        return redirect()->route('user.your_posts')->with('message', 'Blog has been deleted!');
    }
}
