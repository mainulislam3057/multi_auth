<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    // function create(Request $request){
    //     //Validate Inputs
    //     $request->validate([
    //         'name'=>'required',
    //         'email'=>'required|email|unique:users,email',
    //         'password'=>'required|min:5|max:30',
    //         'cpassword'=>'required|min:5|max:30|same:password'
    //     ]);

    //     User::create([
    //         'name' => $request->name,
    //         'email' => $request->email,
    //         'password' => Hash::make($request->password),
    //     ]);
    //     return redirect()->back()->with('success','You are now registered successfully');
    // }

    // function check(Request $request){
    //     //Validate inputs
    //     $request->validate([
    //        'email'=>'required|email|exists:users,email',
    //        'password'=>'required|min:5|max:30'
    //     ],[
    //         'email.exists'=>'This email is not exists on users table'
    //     ]);

    //     $creds = $request->only('email','password');
    //     if( Auth::guard('web')->attempt($creds) ){
    //         return back();
    //     }else{
    //         return redirect()->route('user.login')->with('fail','Incorrect credentials');
    //     }
    // }

    // function logout(){
    //     Auth::guard('web')->logout();
    //     return redirect('/');
    // }

    public function blogShow()
    {
        $user_id = Auth::user()->id;
        $posts = Post::where('user_id', $user_id)->get();
        return view('dashboard.user.your_posts', [
            'posts' => $posts
        ]);;
    }

    public function countShow()
    {
        $user_id = auth()->user()->id;
        $post_count = Post::where('user_id', $user_id)->count();

        $comment_count = Comment::where('user_id', $user_id)->count();
     
        return view('dashboard.user.home', [
            'post_count' => $post_count,
            'comment_count' => $comment_count,
        ]);

    }
}