@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="row">
     
      @include('layouts.sidebar')

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
       <div class="conatiner">
          <div class="row">
              <div class="col-md-12">
                @if (Session::get('message'))
                <div class="alert alert-success">
                    {{ Session::get('message') }}
                </div>
            @endif
                <table class="table table-striped" id="myTable_manage">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Published</th>
                        <th>Approval</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($blogs as $blog)
                        <tr>
                            <td>{{ \Illuminate\Support\Str::limit($blog->title, 30, $end='...')}}</td>
                            <td>{{ucfirst($blog->user->name)}}</td>
                            <td>{{ date('jS M Y', strtotime($blog->created_at)) }}</td>
                            <td>@if ($blog->status == 1)
                              <span class="badge badge-pill badge-primary">Approved</span>
                            @elseif ($blog->status == 2)
                            <span class="badge badge-pill badge-danger">Declined</span>
                            @elseif ($blog->status == 0)
                            <span class="badge badge-pill badge-warning">Pending</span>
                            @endif</td>
                            <td class="text-ceneter"><a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal2{{$blog->id}}">Edit</a> <a href="{{route('admin.single_post', $blog->id)}}" class="btn btn-success btn-sm">View</a> <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal{{$blog->id}}">Delete</a></td>
                          </tr>
  <!-- The Modal Delete -->
  <div class="modal fade" id="myModal{{$blog->id}}">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Blog</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Are you sure want to delete?
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <form action="{{route('admin.post_delete', $blog->id)}}" method="post">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <!-- The Modal Status Change -->
  <div class="modal fade" id="myModal2{{$blog->id}}">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Approval of Blog</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form action="{{route('admin.post_status', $blog->id)}}" method="post">
            @csrf
          <div class="form-group">
            <label for="sel1">Select Status:</label>
          <select name="status" id="" class="form-control">
            <option value="0">Pending</option>
            <option value="1">Approve</option>
            <option value="2">Decline</option>
          </select>
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger">Submit</button>
          </form>
        </div>
        
      </div>
    </div>
  </div>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
       </div>
      </main>
    </div>
  </div>

  
@endsection