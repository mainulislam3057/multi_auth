@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="row">
     
      @include('layouts.sidebar')

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
       <div class="conatiner">
          <div class="row">
              <div class="col-md-12">
                @if (Session::get('message'))
                <div class="alert alert-success">
                    {{ Session::get('message') }}
                </div>
            @endif
                <table class="table table-striped" id="myTable">
                    <thead>
                      <tr>
                        <th>Author Name</th>
                        <th>Email</th>
                        <th>Join at</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ucfirst($user->name)}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{ date('jS M Y', strtotime($user->created_at)) }}</td>
                            <td><a href="" class="btn btn-danger" data-toggle="modal" data-target="#myModal{{$user->id}}">Delete</a></td>
                          </tr>
  <!-- The Modal -->
  <div class="modal fade" id="myModal{{$user->id}}">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Are you sure want to delete?
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <form action="{{route('admin.user_delete', $user->id)}}" method="post">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </div>
        
      </div>
    </div>
  </div>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
       </div>
      </main>
    </div>
  </div>

  
@endsection