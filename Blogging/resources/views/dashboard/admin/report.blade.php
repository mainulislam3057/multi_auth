@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="row">
     
      @include('layouts.sidebar')

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
       <div class="conatiner">
          <div class="row">
              <div class="col-md-12">
                @if (Session::get('message'))
                <div class="alert alert-success">
                    {{ Session::get('message') }}
                </div>
            @endif
                <form action="{{route('admin.report_get')}}" method="get">
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker'>
                        <div class="col">
                            <input type='date' class="form-control"  name="from" value="{{request()->from}}"/>
                        </div>
                        <div class="col">
                            <input type='date' class="form-control" name="to" value="{{request()->to}}"/>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='input-group'>
                            <div class="col">
                                <select name="user" id="" class="form-control">
                                    @foreach ($users as $user)
                                        <option value="{{$user->id}}" @if(request()->user == $user->id) selected @endif>{{ucfirst($user->name)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <select name="status" id="" class="form-control">
                                        <option value="0" @if(request()->status == '0') selected @endif>Pending</option>
                                        <option value="1" @if(request()->status == '1') selected @endif>Approved</option>
                                        <option value="2" @if(request()->status == '2') selected @endif>Declined</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class='input-group'>
                            <div class="col">
                                <select name="type" id="" class="form-control">
                                    <option value="post" @if(request()->type == 'post') selected @endif>Posts</option>
                                    <option value="comment" @if(request()->type == 'comment') selected @endif>Comments</option>
                            </select>
                            </div>
                            <div class="col">
                                <input type="submit" class="btn btn-primary btn-block" value="Submit">
                            </div>
                        </div>
                    </div>

                </form>

                <table class="table table-striped" id="myTable">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Status</th>
                        <th>Comments</th>
                      </tr>
                    </thead>
                    <tbody>

                        @foreach ($results as $result)
                        <tr>
                            <td>{{$result->title}}</td>
                            <td>{{ucfirst($result->user->name)}}</td>
                            <td>@if ($result->status == 0)
                                Pending
                            @elseif ($result->status == 1)
                                Approved
                            @elseif ($result->status == 2)
                                Declined
                            @endif</td>
                            <td>{{$result->comments_count}}</td>
                         </tr>
                        @endforeach

                    </tbody>
                  </table>
                {{$results->withQueryString()->links()}}
              </div>
          </div>
       </div>
      </main>
    </div>
  </div>

  
@endsection