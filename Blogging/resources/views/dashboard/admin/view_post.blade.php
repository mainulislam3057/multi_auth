@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="row">
     
      @include('layouts.sidebar')

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
       <div class="conatiner">
          <div class="row">
              <div class="col-md-12">
                <div class="conatiner">
                  <div class="row">
                   
                      <div class="col-md-12">
                        @if (Session::get('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                        <div class="jumbotron">
                            <div class="container">
                              <h1 class="display-3">{{$post->title}}</h1>
                              <p>Posted By: {{ ucfirst($post->user->name) }}</p>
                              <p>Published On: {{ date('jS M Y', strtotime($post->created_at)) }}</p>
                              @if (isset(Auth::user()->id) && Auth::user()->id == $post->user_id)
                              <form action="{{route('user.blog.destroy', $post->slug)}}" method="post">
                                @csrf
                                @method('delete')
                               <button type="submit" class="btn btn-danger">Delete Blog</button>
                              </form>                          
                              @endif
                              <hr>
                              <p>{{$post->description}}</p>
                              <hr>
                              @guest
                                  <h6 style="text-align: center; padding-top:15px">Please <a href="{{route('user.login')}}"><b>Log in</b></a> to comment<h6>
                              @endguest
                            </div>
                          </div>
                          <h4>Comments</h4>
                          @foreach ($post->comments as $comment)
                          <div class="card mb-2">
                            <div class="card-body">
                              <h4 class="card-title">{{ucfirst($comment->user->name)}}</h4>
                              <p class="card-text">{{$comment->comment}}</p>
                               @if (isset(Auth::user()->id) && Auth::user()->id == $comment->user_id && (!auth()->user()->isAdmin))
                               <button type="submit" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal{{$comment->id}}">Edit</button>
                               <form action="{{route('comment.destroy', $comment->id)}}" method="post">
                                 @csrf
                                 @method('delete')
                                 <button type="submit" class="btn btn-danger btn-sm mt-2">Delete</button>
                              </form>                          
                              @endif
                            </div>
                          </div>
                          <!-- The Modal -->
        <div class="modal" id="myModal{{$comment->id}}">
          <div class="modal-dialog">
            <div class="modal-content">
        
              <!-- Modal Header -->
              <div class="modal-header">
                <h4 class="modal-title">Your Comment</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
        
              <!-- Modal body -->
              <div class="modal-body">
                <form action="{{route('comment.update', $comment->id)}}" method="post">
                  @csrf
                  @method('PUT')
                <textarea name="comment" id="" cols="30" rows="10" class="form-control">{{$comment->comment}}</textarea>
                <span class="text-danger">@error('comment'){{ $message }}@enderror</span>
              </div>
        
              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
              </form>
        
            </div>
          </div>
        </div>
        <!-- The Modal END -->
                          @endforeach
                      </div>
                  </div>
        
        
              </div>
          </div>
       </div>
      </main>
    </div>
  </div>

@endsection