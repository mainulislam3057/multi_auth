@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="row">
     
      @include('layouts.sidebar')

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
       <div class="conatiner">
          <div class="row">
              <div class="col-md-12">
                  @if (Session::get('message'))
                      <div class="alert alert-success">
                          {{ Session::get('message') }}
                      </div>
                  @endif
                  <form action="{{route('user.blog.store')}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                          <label for="title">Title</label>
                          <input type="text" name="title" class="form-control">
                          <span class="text-danger">@error('title'){{ $message }}@enderror</span>
                      </div>
                      <div class="form-group">
                          <label for="title">Description</label>
                          <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
                          <span class="text-danger">@error('description'){{ $message }}@enderror</span>
                      </div>
                      <div class="form-group">
                          <label for="title">Image</label><br>
                          <img id="image" name="image" style="width:300px;" class="normal" />
                          <input type="file" name="image" class="form-control" id="files">
                          <span class="text-danger">@error('image'){{ $message }}@enderror</span>
                      </div>
                      <button type="submit" class="btn btn-primary btn-block">Submit</button>
                  </form>
              </div>
          </div>
       </div>
      </main>
    </div>
  </div>

@endsection