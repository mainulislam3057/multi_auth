@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="row">
     
      @include('layouts.sidebar')

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
       <div class="conatiner">
          <div class="row">
              <div class="col-md-12">
                <div class="card-columns">
                  <div class="card bg-primary">
                    <div class="card-body text-center">
                      <p class="card-text text-white">Total {{$post_count}} Posts</p>
                    </div>
                  </div>
                  <div class="card bg-warning">
                    <div class="card-body text-center">
                      <p class="card-text">Some text inside the second card</p>
                    </div>
                  </div>
                  <div class="card bg-success">
                    <div class="card-body text-center">
                      <p class="card-text">Total {{$comment_count}} Comments</p>
                    </div>
                  </div>
                  <div class="card bg-danger">
                    <div class="card-body text-center">
                      <p class="card-text">Some text inside the fourth card</p>
                    </div>
                  </div>
                  <div class="card bg-light">
                    <div class="card-body text-center">
                      <p class="card-text">Some text inside the fifth card</p>
                    </div>
                  </div>
                  <div class="card bg-info">
                    <div class="card-body text-center">
                      <p class="card-text">Some text inside the sixth card</p>
                    </div>
                  </div>
                </div>
              </div>
          </div>
       </div>
      </main>
    </div>
  </div>

@endsection