@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <div class="row">
     
      @include('layouts.sidebar')

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
       <div class="conatiner">
          <div class="row">
              <div class="col-md-12">
                @if (Session::get('message'))
                <div class="alert alert-success">
                    {{ Session::get('message') }}
                </div>
                @endif
                <div class="album py-5 bg-light">
                  <div class="container">
                    <div class="row">
                      @foreach ($posts as $post)
                      <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                          <img class="card-img-top" src="{{ asset('storage/images/'.$post->image_path)}}" height="200px">                          
                          <div class="card-body">
                            <h6 class="card-text">{{ $post->title }}</h6>
                            <p>{{ \Illuminate\Support\Str::limit($post->description, 30, $end='...')}}</p>
                            <div class="d-flex justify-content-between align-items-center">
                              <div class="btn-group">
                                <a href="{{route('user.blog.show', $post->slug)}}" class="btn btn-sm btn-outline-secondary">View</a>
                                <a href="{{route('user.blog.edit', $post->slug)}}" class="btn btn-sm btn-outline-secondary">Edit</a>
                              </div>
                              <small class="text-muted">{{ date('jS M Y', strtotime($post->created_at)) }}</small>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
      </main>
    </div>
  </div>

@endsection

