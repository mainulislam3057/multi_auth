<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
      <ul class="nav flex-column">
        @if (auth('web')->check())
        <li class="nav-item">
          <a class="nav-link active" href="{{route('user.home')}}">
            <span data-feather="home"></span>
            Dashboard <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('user.your_posts') }}">
            <span data-feather="book"></span>
            Your Posts
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('user.blog.create') }}">
            <span data-feather="file-plus"></span>
            Create new Blog
          </a>
        </li>
        @elseif(auth('admin')->check())
        <li class="nav-item">
          <a class="nav-link active" href="">
            <span data-feather="home"></span>
            Dashboard <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('admin.manage_posts')}}">
            <span data-feather="book"></span>
            Manage Posts
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('admin.manage_users')}}">
            <span data-feather="user"></span>
            Manage Users
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('admin.report_get')}}">
            <span data-feather="user"></span>
            Day wise Reports
          </a>
        </li>
        @endif
      </ul>
    </div>
  </nav>