@extends('layouts.master')
@section('content')
    
    <body class="antialiased">
        {{-- {{ auth('web')->check()  ? auth('web')->id():"Web not found" }}
        {{ auth('admin')->check()  ? auth('admin')->id():"Web not found" }} --}}
        {{-- <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                    @else
                        <a href="{{ route('user.login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('user.register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif --}}

            <div class="conatiner">
                <div class="row">
                    <div class="col-md-12">
                      @if (Session::get('message'))
                      <div class="alert alert-success">
                          {{ Session::get('message') }}
                      </div>
                      @endif
                      <div class="album py-5 bg-light">
                        <div class="container">
                          <div class="row">
                            @foreach ($posts as $post)
                            <div class="col-md-4">
                              <div class="card mb-4 box-shadow">
                                <img class="card-img-top" src="{{ asset('storage/images/'.$post->image_path)}}" height="200px">                          
                                <div class="card-body">
                                  <h6 class="card-text">{{ \Illuminate\Support\Str::limit($post->title, 30, $end='...')}}</h6>
                                  <span style="color: lightslategrey;">Author: <span>{{ucfirst($post->user->name)}} -</span> Comments: <span>{{$post->comments->count()}}</span></span>
                                  <p>{{ \Illuminate\Support\Str::limit($post->description, 30, $end='...')}}</p>
                                  <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                      <a href="{{route('view_blog', $post->slug)}}" class="btn btn-sm btn-outline-secondary">View</a>
                                    </div>
                                    <small class="text-muted">{{ date('jS M Y', strtotime($post->created_at)) }}</small>
                                  </div>
                                </div>
                              </div>
                            </div>
                            @endforeach
                          </div>
                        </div>
                      </div>
        </div>
@endsection