<?php

use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\GuestPostController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\User\UserAuthController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GuestPostController::class, 'index'])->name('index');
Route::get('/view_blog/{post}', [GuestPostController::class, 'show'])->name('view_blog');
Route::resource('/comment', CommentController::class);



Auth::routes();

// USER ROUTES
Route::prefix('user')->name('user.')->group(function(){

    Route::middleware(['guest:web'])->group(function(){
        Route::view('/login', 'dashboard.user.login')->name('login');
        Route::view('/register', 'dashboard.user.register')->name('register');
        Route::post('/create', [UserAuthController::class, 'create'])->name('create');
        Route::post('/login', [UserAuthController::class, 'submit'])->name('submit');

    });

    Route::middleware(['auth:web'])->group(function(){
        Route::get('/home', [UserController::class, 'countShow'])->name('home');
        Route::resource('/blog', PostController::class);
        Route::post('/logout', [UserAuthController::class, 'logout'])->name('logout');
        Route::get('your-posts', [UserController::class, 'blogShow'])->name('your_posts');
        // Route::view('create-blog', 'dashboard.user.create_blog')->name('create_blog');
    });
});

// ADMIN ROUTES
Route::prefix('admin')->name('admin.')->group(function(){
       
    Route::middleware(['guest:admin'])->group(function(){
          Route::view('/login','dashboard.admin.login')->name('login');
          Route::post('/login',[AdminAuthController::class,'submit'])->name('submit');
    });

    Route::middleware(['auth:admin'])->group(function(){
        Route::view('/home','dashboard.admin.home')->name('home');
        Route::get('/manage_posts', [AdminController::class, 'show_posts'])->name('manage_posts');
        Route::get('/manage_users', [AdminController::class, 'show_users'])->name('manage_users');
        Route::get('/single_post/{id}', [AdminController::class, 'single_post'])->name('single_post');
        Route::delete('/post_delete/{id}', [AdminController::class, 'post_delete'])->name('post_delete');
        Route::delete('/user_delete/{id}', [AdminController::class, 'user_delete'])->name('user_delete');
        Route::post('/post_status/{id}', [AdminController::class, 'post_status'])->name('post_status');
        Route::get('/day-wise-report', [AdminController::class, 'report_get'])->name('report_get');
        // Route::post('/day-wise-report', [AdminController::class, 'report_result'])->name('report_result');
        Route::post('/logout',[AdminAuthController::class,'logout'])->name('logout');
    });

});